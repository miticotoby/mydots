#!/bin/bash

echo "## use mydev..."
exit 1

#MYPATH=$(realpath --relative-to="$HOME" -s $(dirname $0))
cd $(dirname $0)

git submodule update --init --recursive

MYPATH=$(pwd)

cd $HOME

#rm .bash_aliases .bash-git-prompt .gitconfig .git-prompt-colors.sh .screenrc .tmux.conf .vim

ln -is $MYPATH/.bash_aliases
ln -is $MYPATH/.bash-git-prompt
ln -is $MYPATH/.gitconfig
ln -is $MYPATH/.git-prompt-colors.sh
ln -is $MYPATH/.screenrc
ln -is $MYPATH/.tmux.conf
ln -is $MYPATH/.vim

## testing neovim
mkdir -p .config/nvim
ln -s ../../.vim/vimrc .config/nvim/init.vim
mkdir -p .local/share/nvim/site
ln -s ../../../../.vim/pack .local/share/nvim/site/pack

echo
echo '######################################################################################'
echo '###   for full vim/tmux airline support you should install following packages      ###'
echo '###   apt-get install fonts-powerline                                              ###'
echo '###   you can also disable powerline font in .vim/airline.vim                      ###'
echo '###                                                                                ###'
echo '###   for full vim IDE consider enabling coc pluging                               ###'
echo '###   ln -s ../coc.nvim/ .vim/pack/myplugins/start/coc.nvim                        ###'
echo '###                                                                                ###'
echo '###   if running GNOME, run bin/gnome-tracker-disable and bin/gnome-disable-bell   ###'
echo '######################################################################################'
echo '###                                                                                ###'
echo '###   consider the keys below to add to your authorized_keys file                  ###'
echo
cat $MYPATH/authorized_keys
echo
echo '######################################################################################'
