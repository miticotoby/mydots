#!/bin/bash
cd $(dirname $0)

echo '## this will *remove* .git .gitmodules foxyproxy.json .ssh/config.lazog'
echo '## are you OK with that?'
echo 'be sure....'
read SURE
[ "$SURE" == "yes" ] || exit 1

rm -rf .git .gitmodules foxyproxy.json .ssh/config.lazog

sed -i '/signingkey/d' .gitconfig
sed -i '/config.lazog/d' .ssh/config.lazog
