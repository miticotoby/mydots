#!/bin/bash
set -Eeuo pipefail

id=${1}
descr=${2}

## wg-gen.conf
## myend="1.2.3.4:1234"
## mypub="asdfasdfasdfasdfasdfasdfasdfasdfasdfasfdasf="
## mydns="2000:1234:4321:1234::1"
## myv4="172.16.0.${id}"
## myv6="2000:1234:4321:1234::${id}"
##
source wg-gen.conf

seq=30000
mytable=1234
psk=$(wg genpsk)
key=$(wg genkey)
pub=$(wg pubkey <<<"$key")


## client config
cat <<-EOF >wg-srv-${descr}.conf
## ${descr}
[Peer]
PublicKey = ${pub}
PresharedKey = ${psk}
AllowedIPs = ${myv4}, ${myv6}
PersistentKeepalive = 25
EOF


## server config
cat <<-EOF >wg-client-${descr}.conf
## ${HOSTNAME}
[Interface]
PrivateKey = ${key}
Address = ${myv4}, ${myv6}
DNS = ${mydns}
FwMark = off
Table = ${mytable}
PostUp = ip -4 rule add lookup main suppress_prefixlength 0 pref ${seq}
PostUp = ip -4 rule add not to ${myend%:*}/32 ipproto udp dport ${myend#*:} lookup ${mytable} pref $((seq+10))
PostUp = ip -6 rule add lookup main suppress_prefixlength 0 pref ${seq}
PostUp = ip -6 rule add lookup ${mytable} pref $((seq+10))
PreDown = ip -4 rule del lookup main suppress_prefixlength 0 pref ${seq}
PreDown = ip -4 rule del not to ${myend%:*}/32 ipproto udp dport ${myend#*:} lookup ${mytable} pref $((seq+10))
PreDown = ip -6 rule del lookup main suppress_prefixlength 0 pref ${seq}
PreDown = ip -6 rule del lookup ${mytable} pref $((seq+10))

[Peer]
PresharedKey = ${psk}
PublicKey = ${mypub}
AllowedIPs = ::/0, 0.0.0.0/0
Endpoint = ${myend}
PersistentKeepalive = 25
EOF

## print qrcode
cat wg-client-${descr}.conf | qrencode -t ansiutf8

echo "Add server config: wg addconf <ifi> wg-srv-${descr}.conf"
echo "Add client config: scan qrcode or copy wg-client-${descr}.conf"
