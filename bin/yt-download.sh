#!/bin/bash
set -Eeuo pipefail

case "${1:-}" in
	## audio only
	audio)  yt-dlp -f "ba" -o "%(playlist_index)s - %(title)s.%(ext)s" --extract-audio --audio-format mp3 "$2" ;;

	## audio+video
	video)	yt-dlp -f "bv+ba/b" --merge-output-format mkv/mp4 "$2" ;;

	*)	echo "unknown option" && exit 1 ;;
esac
