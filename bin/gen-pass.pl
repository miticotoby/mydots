#!/usr/bin/perl
use strict;
my $length;
if ( $ARGV[0] ) {
   $length=$ARGV[0];
} else {
   $length=25;
}

sub rndStr{ join'', @_[ map{ rand @_ } 1 .. shift ] }
#my $pass = rndStr 12, 'A'..'Z', 0..9, 'a'..'z';
my $pass = rndStr $length, 'A'..'Z', 0..9, 'a'..'z', '.', '-', '_', '$', '#', '!', '?', '%', '@', '+', '(', ')';
#my $pass = rndStr $length, 'A'..'Z', 0..9, 'a'..'z', '-', '_', '.';
print "$pass\n"
