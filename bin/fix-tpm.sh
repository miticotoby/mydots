#!/bin/bash
set -Eeuo pipefail

dsk=$(sudo cat /etc/crypttab | grep -v "^#" |  awk '{ print $2 }')
fido="--unlock-fido2-device=auto"

if [ "${1:-}" == "nofidounlock" ]; then
	echo "not using fido2 to unlock"
	fido=""
fi

for i in ${dsk}; do
	d=$(blkid --uuid ${i##UUID=})
	echo "re-enrolling $d"
	sudo systemd-cryptenroll --wipe-slot=tpm2 --tpm2-device=auto --tpm2-with-pin=yes --tpm2-pcrs=0+4+5+7+9 ${d} ${fido}
	#sudo systemd-cryptenroll --wipe-slot=tpm2 --tpm2-device=auto --tpm2-with-pin=yes --tpm2-pcrs=0+4+5+7+9 ${d} --unlock-fido2-device=auto
	#sudo systemd-cryptenroll --wipe-slot=fido2 --fido2-device=auto --fido2-with-user-presence=true ${d}
	#sudo systemd-cryptenroll --wipe-slot=fido2 --fido2-device=auto --fido2-with-user-presence=true --fido2-with-user-verification=true ${d}
done
