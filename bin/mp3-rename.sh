#!/bin/bash

for i in *.mp3; do
	artist=$(id3v2 -l "${i}" | grep TPE1 | sed 's/.*: //')
	album=$(id3v2 -l "${i}" | grep TALB | sed 's/.*: //')
	title=$(id3v2 -l "${i}" | grep TIT2 | sed 's/.*: //')
	track=$(id3v2 -l "${i}" | grep TRCK | sed 's/.*: //')
	track="$(printf "%02d" ${track})"

	mv -i "${i}" "${artist} - ${album} - ${track} - ${title}.mp3"
done
