#!/bin/bash
set -Eeuo pipefail

: ${SQLFILE:=${1}}
: ${TXTFILE:=${SQLFILE%.sqlite}.txt}

if [ ! -e "${SQLFILE}" ]; then
	echo "${SQLFILE} not found"
	exit 1
fi

if [ -e "${TXTFILE}" ]; then
	echo "${TXTFILE} already exists"
	exit 1
fi

echo "# Netscape HTTP Cookie File" >"${TXTFILE}"
sqlite3 -separator $'\t' ${SQLFILE} >>"${TXTFILE}" <<- EOF
	.mode tabs
	.header off
	select host,
	case substr(host,1,1)='.' when 0 then 'FALSE' else 'TRUE' end,
	path,
	case isSecure when 0 then 'FALSE' else 'TRUE' end,
	expiry,
	name,
	value
	from moz_cookies;
EOF
