#!/bin/bash
set -Eeuo pipefail

artist="${MY_ARTIST}"
album="${MY_ALBUM}"
year="${MY_YEAR}"
genere="${MY_GENERE}"


for i in *.mp3; do

	song="${i##* - }"
	track="${i% - *}"

	echo "##  tagging ${artist} - ${album} - ${track} - ${song}"

	#id3v2 \
	#	--artist "${artist}" \
	#	--album "${album}" \
	#	--year "${year}" \
	#	--genere "${genere}" \
	#	--song "${song}" \
	#	--comment mitico \
	#	"$i"

	echo id3v2 \
		--artist="${artist}" \
		--album="${album}" \
		--TYER="${year}" \
		--TIT2="${song}" \
		--COMM=mitico \
		--track="${track}" \
		"$i"
done
